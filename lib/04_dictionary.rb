class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(entries)
    if entries.is_a?(Hash)
      @entries.merge!(entries)
    else
      @entries[entries] = nil
    end

  end

  def keywords
    @entries.keys.sort {|el1, el2| el1 <=> el2}
  end

  def include?(key)
    @entries.has_key?(key)
  end

  def find(prefix)
    @entries.select do |key, value|
      key.start_with?(prefix)
    end
  end

  def printable
    output = []
    self.keywords.each do |key|
      output << %Q([#{key}] "#{@entries[key]}")
    end

    output.join("\n")
  end
end
