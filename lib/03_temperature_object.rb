class Temperature
  def initialize(opt = {})
    @opt = opt
  end

  def in_fahrenheit
    @opt.has_key?(:f) ? @opt[:f] : (@opt[:c] * 9.0/5) + 32
  end

  def in_celsius
    @opt.has_key?(:f) ? (@opt[:f]-32) * 5.0/9 : @opt[:c]
  end

  def self.from_celsius(temp)
    self.new(:c => temp)
  end

  def self.from_fahrenheit(temp)
    self.new(:f => temp)
  end
end


class Celsius < Temperature
  def initialize(temp, opt={})
    @opt = {}
    @opt[:c] = temp
  end

  def in_fahrenheit
    super
  end

  def in_celsius
    super
  end
end


class Fahrenheit < Temperature
  def initialize(temp, opt = {})
    @opt = opt
    @opt[:f] = temp
  end

  def in_fahrenheit
    super
  end
end
