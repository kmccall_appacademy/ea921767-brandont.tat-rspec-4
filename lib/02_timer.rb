class Timer
  attr_accessor :seconds

  def initialize(sec = 0)
    @seconds = sec
  end

  def hours
    @seconds/3600
  end

  def minutes
    (@seconds%3600)/60
  end

  def fix_seconds
    @seconds%60
  end

  def time_string
    @time_string = "#{padded(hours)}:#{padded(minutes)}:#{padded(fix_seconds)}"
  end

  def padded(time)
    time < 10 ? "0#{time}" : time
  end
end
