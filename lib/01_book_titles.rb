
class Book
  attr_reader :title

  @@lowercase = %w(the and in of a an)

  def title=(title)
    title_words = title.split(' ').map(&:downcase)

    fixed_title = title_words.map.with_index do |word, i|
      if @@lowercase.include?(word) && i != 0
        word
      else
        word.capitalize
      end
    end

    @title = fixed_title.join(' ')
  end
end
